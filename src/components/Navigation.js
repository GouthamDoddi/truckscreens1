import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'

class Navigation extends Component {
    render() {
    return (
        <div>
            <NavLink to="/">Image </NavLink>
            <NavLink to="/first"> first </NavLink>
            <NavLink to="/second"> Second </NavLink>
            <NavLink to="/third"> Third </NavLink>
            <NavLink to="/fourth"> Fourth </NavLink>
        </div>
    )
    }
}

export default Navigation;