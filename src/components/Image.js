import React, { Component } from 'react';
import AppImage from './AppImage.png';

class Image extends Component {
    render() {
    return (
        <div>
            <img src={AppImage} className="App-Image"/>
        </div>
    );
    }
}

export default Image;