import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'

class Navigation2 extends Component {
    render() {
    return (
        <div>
            <NavLink to="/">Image </NavLink>
            <NavLink to="/transportfirst"> Transportfirst </NavLink>
            <NavLink to="/transportsecond"> Transportsecond </NavLink>
            <NavLink to="/transportthird"> Transportthird </NavLink>
        </div>
    )
    }
}

export default Navigation2;
