import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'

class Navigation1 extends Component {
    render() {
    return (
        <div>
            <NavLink to="/">Image </NavLink>
            <NavLink to="/truckfirst"> Truckfirst </NavLink>
            <NavLink to="/trucksecond"> Trucksecond </NavLink>
            <NavLink to="/truckthird"> Truckthird </NavLink>
        </div>
    )
    }
}

export default Navigation1;
